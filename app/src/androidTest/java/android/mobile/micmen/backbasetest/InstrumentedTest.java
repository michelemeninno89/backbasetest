package android.mobile.micmen.backbasetest;

import android.content.Context;
import android.content.res.AssetManager;
import android.mobile.micmen.backbasetest.model.City;
import android.mobile.micmen.backbasetest.utils.AssetUtil;
import android.mobile.micmen.backbasetest.utils.CitiesFilterUtil;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Filtering cities test
 *
 *@author michele
 */
@RunWith(AndroidJUnit4.class)
public class InstrumentedTest {

    private static List<City>  cities;

    @BeforeClass
    public  static void readCitiesFromAsset(){
        Context context = InstrumentationRegistry.getTargetContext();
        AssetManager assets = context.getAssets();
        Gson gson = new Gson();
        cities = Arrays.asList(AssetUtil.loadJson("cities.json", City[].class, assets, gson));
        Collections.sort(cities, new Comparator<City>() {
            @Override
            public int compare(City o1, City o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    @Test
    public void citiesFilteringTestWithPrefix(){
        String prefix = "A";
        List<City> filteredList = CitiesFilterUtil.retrieveListFilteredByPrefix(prefix, cities);
        for (City city : filteredList) {
            Assert.assertTrue(city.getName().toLowerCase().startsWith(prefix.toLowerCase()));
        }
    }

    @Test
    public void citiesFilteringTestNoPrefix(){
        String prefix = "";
        List<City> filteredList = CitiesFilterUtil.retrieveListFilteredByPrefix(prefix, cities);
        Assert.assertTrue(filteredList.size() == cities.size());

    }


    @Test
    public void citiesFilteringTestInvalidPrefix(){
        String prefix = "#####";
        List<City> filteredList = CitiesFilterUtil.retrieveListFilteredByPrefix(prefix, cities);
        Assert.assertTrue(filteredList.size() ==0);

    }


}
