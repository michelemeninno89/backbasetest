package android.mobile.micmen.backbasetest.feature.cities.adaper;

import android.mobile.micmen.backbasetest.R;
import android.mobile.micmen.backbasetest.model.City;
import android.mobile.micmen.backbasetest.utils.CitiesFilterUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.List;

/**
 * <h1>CitiesAdapter</h1>
 * Filterable Adapter for the city list
 *
 * @author michele meninno
 */

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.CityViewHolder> implements Filterable {

    private List<City> originalList;

    public void setCityClickListener(OnCityClickListener cityClickListener) {
        this.cityClickListener = cityClickListener;
    }

    private OnCityClickListener cityClickListener;

    public List<City> getCityListFiltered() {
        return cityListFiltered;
    }

    private List<City> cityListFiltered;

    public CitiesAdapter(List<City> cities) {
        originalList = cities;
        cityListFiltered = originalList;
    }

    @NonNull
    @Override
    public CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.city_element, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        holder.onBind(cityListFiltered.get(position));
    }

    @Override
    public int getItemCount() {
        return cityListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new CitiesFilter();
    }


    class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textView;
        private City city;

        public CityViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView = itemView.findViewById(R.id.text);
        }

        @Override
        public void onClick(View v) {
            if (cityClickListener != null) {
                cityClickListener.onCityClicked(city);
            }
        }

        public void onBind(City city) {
            this.city = city;
            textView.setText(city.getName().concat(" , ").concat(city.getCountry()));
        }
    }

    public class CitiesFilter extends Filter {

        public CitiesFilter() {
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Log.d(CitiesAdapter.class.getSimpleName(), "constraint typed :" + constraint.toString());
            FilterResults filterResults = new FilterResults();
            filterResults.values = CitiesFilterUtil.retrieveListFilteredByPrefix(constraint, originalList);
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            Log.d(CitiesAdapter.class.getSimpleName(), "constraint received :" + constraint.toString());
            cityListFiltered = (List<City>) results.values;
            notifyDataSetChanged();
        }
    }

    public interface OnCityClickListener {
        void onCityClicked(City city);
    }


}
