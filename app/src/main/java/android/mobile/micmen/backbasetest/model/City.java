
package android.mobile.micmen.backbasetest.model;

import com.google.gson.annotations.SerializedName;

public class City {

    @SerializedName("coord")
    private Coord mCoord;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("name")
    private String mName;
    @SerializedName("_id")
    private Long m_id;

    public Coord getCoord() {
        return mCoord;
    }

    public void setCoord(Coord coord) {
        mCoord = coord;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Long get_id() {
        return m_id;
    }

    public void set_id(Long _id) {
        m_id = _id;
    }

}
