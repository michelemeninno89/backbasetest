package android.mobile.micmen.backbasetest;

import android.mobile.micmen.backbasetest.feature.cities.CitiesListFragment;
import android.mobile.micmen.backbasetest.feature.cities.CitiesListManager;
import android.mobile.micmen.backbasetest.feature.map.MapFragment;
import android.mobile.micmen.backbasetest.model.City;
import android.mobile.micmen.backbasetest.model.Coord;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

/**
 *<h1>Main Activity</h1>
 *
 * this class manages the toolbar and  the fragments (providing them the data)
 *
 * @author michele meninno
 */
public class MainActivity extends AppCompatActivity implements CitiesListFragment.CommunicationListener {

    private ImageView backButton;
    private CitiesListManager manager;
    private CitiesListFragment citiesListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backButton = findViewById(R.id.back_button);
        manager = new CitiesListManager(listener);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCutomBackPressed();
            }
        });
        switchToCityListFragment();
    }

    private void onCutomBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            backButton.setVisibility(View.INVISIBLE);
            getSupportFragmentManager().popBackStackImmediate();
        }
        else {
            super.onBackPressed();
        }
    }

    public void switchToCityListFragment() {
        citiesListFragment = CitiesListFragment.newInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, citiesListFragment, CitiesListFragment.TAG);
        fragmentTransaction.commit();
    }

    public void switchToMapFragment(Coord coord) {
        MapFragment fragment = MapFragment.newInstance(coord);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, MapFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onCityClicked(Coord coord) {
        backButton.setVisibility(View.VISIBLE);
        switchToMapFragment(coord);
    }

    @Override
    public void onLoadCities() {
        manager.loadCities();
    }

    @Override
    public void onBackPressed() {
        onCutomBackPressed();
    }

    private CitiesListManager.OnCitiesFecthingListener listener = new CitiesListManager.OnCitiesFecthingListener() {
        @Override
        public void onCitiesFetched(List<City> cities) {
            citiesListFragment.setCities(cities);
        }
    };
}
