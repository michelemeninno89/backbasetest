package android.mobile.micmen.backbasetest.feature.cities;

import android.content.Context;
import android.mobile.micmen.backbasetest.R;
import android.mobile.micmen.backbasetest.feature.cities.adaper.CitiesAdapter;
import android.mobile.micmen.backbasetest.model.City;
import android.mobile.micmen.backbasetest.model.Coord;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.List;

/**
 * <h1>CitiesListFragment</h1>
 *
 * works with the activity for loading the data and with the cities' adapter for filtering the list
 *
 * @author michele meninno
 */
public class CitiesListFragment extends Fragment {

    private RecyclerView recyclerView;
    private EditText editText;
    private CitiesAdapter adapter;
    private View progressBar;
    private CommunicationListener communicationListener;
    public static final String TAG = CitiesListFragment.class.getSimpleName();

    public static CitiesListFragment newInstance() {
        Bundle args = new Bundle();
        CitiesListFragment fragment = new CitiesListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            communicationListener = (CommunicationListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement CommunicationListener interface ");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cities_list_fragment, container, false);
        recyclerView = view.findViewById(R.id.cities);
        editText = view.findViewById(R.id.edit_text);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        editText.addTextChangedListener(watcher);
        return view;
    }

    private void loadCities() {
        showProgress();
        communicationListener.onLoadCities();
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadCities();
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d(CitiesListFragment.class.getSimpleName(), "constraint typed :" + s.toString());
            adapter.getFilter().filter(s);
        }
    };

    public void setCities(List<City> cities) {
        hideProgress();
        editText.setEnabled(true);
        adapter = new CitiesAdapter(cities);
        adapter.setCityClickListener(cityClickListener);
        recyclerView.setAdapter(adapter);
    }

    private CitiesAdapter.OnCityClickListener cityClickListener = new CitiesAdapter.OnCityClickListener() {
        @Override
        public void onCityClicked(City city) {
            communicationListener.onCityClicked(city.getCoord());
        }
    };

    public interface CommunicationListener {
        void onCityClicked(Coord coord);

        void onLoadCities();
    }
}
