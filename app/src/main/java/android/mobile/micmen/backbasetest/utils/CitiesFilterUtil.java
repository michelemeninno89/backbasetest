package android.mobile.micmen.backbasetest.utils;

import android.mobile.micmen.backbasetest.model.City;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>CitiesFilterUtil</h1>
 *
 * filtering criteria for the city list
 *
 * @author michele meninno
 */
public class CitiesFilterUtil {

    public static List<City> retrieveListFilteredByPrefix(CharSequence constraint, List<City> originalList) {
        List<City> citiesFiltered = new ArrayList<>();
        if (constraint.length() == 0) {
            citiesFiltered = originalList;
        } else {
            citiesFiltered = new ArrayList<>();
            for (City city : originalList) {
                if (city.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                    citiesFiltered.add(city);
                }
            }
        }
        return citiesFiltered;
    }

}
