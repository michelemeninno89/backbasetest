package android.mobile.micmen.backbasetest.core;

import android.app.Application;
import android.os.Handler;

public class TestApplication extends Application {

    /**
     * provides a reference to the handler
     * used for sending data from a background task to the UI thread
     *
     * @author michele meninno
     */
    private static TestApplication instance;
    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        handler = new Handler();
    }

    public static TestApplication getIstance(){
        return instance;
    }

    public  Handler getHandler(){
        return handler;
    }



}
