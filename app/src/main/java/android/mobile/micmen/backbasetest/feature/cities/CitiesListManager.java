package android.mobile.micmen.backbasetest.feature.cities;

import android.content.res.AssetManager;
import android.mobile.micmen.backbasetest.core.TestApplication;
import android.mobile.micmen.backbasetest.model.City;
import android.mobile.micmen.backbasetest.utils.AssetUtil;
import android.os.Handler;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 * read the list of cities on a background test, caching them
 *
 * @author michele meninno
 */
public class CitiesListManager {

    private final Handler handler;
    private Gson gson;
    private AssetManager assetManager;
    private Executor executor = Executors.newSingleThreadExecutor();
    private OnCitiesFecthingListener listener;
    private List<City> cities;

    public CitiesListManager(OnCitiesFecthingListener listener) {
        this.listener = listener;
        gson = new Gson();
        assetManager = TestApplication.getIstance().getAssets();
        handler = TestApplication.getIstance().getHandler();
    }

    public void loadCities() {

        if (cities == null) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    cities = Arrays.asList(AssetUtil.loadJson("cities.json", City[].class, assetManager, gson));
                    Collections.sort(cities, new Comparator<City>() {
                        @Override
                        public int compare(City o1, City o2) {
                            return o1.getName().compareTo(o2.getName());
                        }
                    });
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onCitiesFetched(cities);
                        }
                    });
                }
            });
            executor.execute(thread);
        } else {
            listener.onCitiesFetched(cities);
        }

    }

    public interface OnCitiesFecthingListener {
        void onCitiesFetched(List<City> cities);
    }


}
